#!/bin/sh

ldoc -d docs -f markdown -p 'Amora Academy' -o utils ./academy/utils.lua
ldoc -d docs -f markdown -p 'Amora Academy' -o controllers ./controllers/*.lua
ldoc -d docs -f markdown -p 'Amora Academy' -o models ./models/*.lua

