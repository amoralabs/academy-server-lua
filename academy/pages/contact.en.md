<!--
title: "Contact"
subtitle: "Do you have any question? Want to reach out for us?"
-->
<form id="inscricao-form" method="POST" action="/academy/contact/">
<label class="label">Name</label>
<p class="control">
<input required class="input" type="text" name="name" placeholder="Fulano de Tal de Orleans e Bragança">
</p>
<label class="label">Subject</label>
<p class="control has-icon has-icon-right">
<input required class="input" type="text" name="subject" placeholder="What is this all about?" value="">
</p>
<label class="label">Email</label>
<p class="control has-icon has-icon-right">
<input required class="input" type="email" name="email" placeholder="hello@exemple.com">
</p>
<label class="label">Phone</label>
<p class="control has-icon has-icon-right">
<input class="input" type="tel" name="tel" placeholder="Your number" value="">
</p>
<label class="label">Message</label>
<p class="control">
<textarea required class="textarea" name="msg" placeholder=""></textarea>
</p>
<p class="control">
<button id="submit-button" class="button is-primary">Send</button>
<button id="cancel-button" class="button is-link">Cancel</button>
</p>
</form>
<script defer src="/pub/contact.js"></script>