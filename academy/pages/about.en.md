<!--
title: "Amora Academy"
subtitle: "Out Of The Box Education!"
-->
The **Amora Academy** is the online learning platform built by **Amora Labs**, a company focused on creating new educational resources for developers, makers, and curious people. We offer free courses accesible by everyone and *premium* courses available only to our subscribers.

## Free Courses
We belive that being able to access quality learning material for free should be a fundamental right. A learner should not be prevented from acquiring new digital skills due to not having enough money. That is why all our basic curriculum is made available for free here at **Amora Academy**. 

## Premium Courses
This premium curriculum offers content and projects beyond basic skills and is available for all subscribers. **All our premium material is available with a subscription of USD 10 per month, which helps us fund this learning platform.**