<!--
title: "Sign Up"
subtitle: "Sign up to be able to enroll in our courses"
-->
<form id="contact-form" method="POST" action="/academy/signup/">
<label class="label">Name</label>
  <p class="control has-icon">
  <input required class="input" type="text" name="name" placeholder="Fulano de Tal de Orleans e Bragança">
  <i class="fa fa-user"></i>
</p>
<label required class="label">Email</label>
  <p class="control has-icon">
  <input class="input" type="email" name="email" placeholder="hello@exemple.com">
  <i class="fa fa-envelope"></i>
</p>
<label required class="label">Confirm Email</label>
  <p class="control has-icon">
  <input class="input" type="email" name="confirm_email" placeholder="Repeat your email address to confirm">
  <i class="fa fa-envelope"></i>
</p>
<label required class="label">Password</label>
<p class="control has-icon">
  <input required class="input" type="password" name="password" placeholder="Password">
  <i class="fa fa-lock"></i>
</p>
<label class="label">Phone</label>
  <p class="control has-icon">
  <input class="input" type="tel" name="tel" placeholder="Your number" value="">
  <i class="fa fa-phone"></i>
</p>
<p class="control">
  <button id="send-contact-button" class="button is-primary">Sign Up</button>
  <button id="cancel-button" class="button is-link">Cancel</button>
</p>
</form>
<script defer src="/pub/signup.js"></script>