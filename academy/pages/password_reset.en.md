<!--
title: "Password Reset"
subtitle: "Err... sometimes we forget our passwords too..."
-->
<form id="password-reset-form" method="POST" action="/academy/password_reset/">
<label required class="label">Email</label>
  <p class="control has-icon">
  <input class="input" type="email" name="email" placeholder="hello@exemplo.com">
  <i class="fa fa-envelope"></i>
</p>
<p class="control">
  <button id="send-contact-button" class="button is-primary">Send</button>
  <button id="cancel-button" class="button is-link">Cancel</button>
</p>
</form>
<script defer src="/pub/password_reset.js"></script>