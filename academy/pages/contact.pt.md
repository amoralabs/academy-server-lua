<!--
title: "Contato"
subtitle: "Tem alguma dúvida? Quer falar com a gente?"
-->
<form id="inscricao-form" method="POST" action="/academy/contact/">
<label class="label">Nome</label>
<p class="control">
<input required class="input" type="text" name="name" placeholder="Fulano de Tal de Orleans e Bragança">
</p>
<label class="label">Assunto</label>
<p class="control has-icon has-icon-right">
<input required class="input" type="text" name="subject" placeholder="Sobre o que que é?" value="">
</p>
<label class="label">Email</label>
<p class="control has-icon has-icon-right">
<input required class="input" type="email" name="email" placeholder="hello@exemplo.com">
</p>
<label class="label">Telefone</label>
<p class="control has-icon has-icon-right">
<input class="input" type="tel" name="tel" placeholder="Seu telefone" value="">
</p>
<label class="label">Mensagem</label>
<p class="control">
<textarea required class="textarea" name="msg" placeholder=""></textarea>
</p>
<p class="control">
<button id="submit-button" class="button is-primary">Enviar</button>
<button id="cancel-button" class="button is-link">Cancelar</button>
</p>
</form>
<script defer src="/pub/contact.js"></script>