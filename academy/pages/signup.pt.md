<!--
title: "Inscrição"
subtitle: "Se inscreva para participar dos cursos"
-->
<form id="contact-form" method="POST" action="/academy/signup/">
<label class="label">Nome</label>
  <p class="control has-icon">
  <input required class="input" type="text" name="name" placeholder="Fulano de Tal de Orleans e Bragança">
  <i class="fa fa-user"></i>
</p>
<label required class="label">Email</label>
  <p class="control has-icon">
  <input class="input" type="email" name="email" placeholder="hello@exemplo.com">
  <i class="fa fa-envelope"></i>
</p>
<label required class="label">Confirmar Email</label>
  <p class="control has-icon">
  <input class="input" type="email" name="confirm_email" placeholder="coloque seu email novamente para confirmar">
  <i class="fa fa-envelope"></i>
</p>
<label required class="label">Senha</label>
<p class="control has-icon">
  <input required class="input" type="password" name="password" placeholder="Password">
  <i class="fa fa-lock"></i>
</p>
<label class="label">Telefone</label>
  <p class="control has-icon">
  <input class="input" type="tel" name="tel" placeholder="Seu telefone" value="">
  <i class="fa fa-phone"></i>
</p>
<p class="control">
  <button id="send-contact-button" class="button is-primary">Enviar</button>
  <button id="cancel-button" class="button is-link">Cancelar</button>
</p>
</form>
<script defer src="/pub/signup.js"></script>