#!env lua

--- readconfig.lua
--
-- Reads an .ini file and display content
--
-- @script readconfig.lua
-- @author Andre Garzia <andre@amoralabs.com>
-- @license MIT
-- @copyright Amora Labs 2016
--
-- @usage readconfig.lua curso-blablabla/course.ini
-- @param path, the file to read.
-- @return table, a pretty representation of the file
--
local config = require 'pl.config'
local pretty = require 'pl.pretty'

local t = config.read(arg[1])
print(pretty.write(t))

