#!env lua
--- scaffold.lua
--
-- Reads an .ini file and creates placeholder files for all markdown needed.
--
-- @script readconfig.lua
-- @author Andre Garzia <andre@amoralabs.com>
-- @license MIT
-- @copyright Amora Labs 2016
--
-- @usage scaffold.lua curso-blablabla/course.ini
-- @param path, the file to read.
--

-- Require all the stuff
require("socket")
local config = require 'pl.config'
local colors = require 'ansicolors'
local path = require "pl.path"
local file = require "pl.file"
local pretty = require "pl.pretty"

local course = config.read(arg[1])

print(colors("%{magentabg}... AMORA SCAFFOLD ...%{reset}"))
print(colors("%{blink blue}Fazendo scaffold de %{bright blue}" .. course.config.name .. "... %{reset}"))
print(colors("%{blink blue}Supported locales: %{bright blue}" .. pretty.write(course.config.available_locales) .. "... %{reset}"))


for i = 1, course.config.units do

    local unit = "unit" .. i

    for _,v in ipairs(course[unit].toc) do
        local item = path.dirname(arg[1]) .. "/" .. v
        file.copy("template.md", item:gsub(".md", ".pt.md"))
        file.copy("template.md", item:gsub(".md", ".en.md"))
        print(colors("%{blink blue}Criando %{bright blue}" .. item .. "... %{reset}"))
    end
end


