--------------------------------------------------------------------------------
--
-- Academy utils
--
-- This is where *dragons* lies. Bad design goes here.
--
-- Academy Utils is the entrypoint for everything related to the courses and
-- auxiliary business logic for Amora Academy.
--
-- @module academy.utils
-- @author Andre Garzia <andre@amoralabs.com>
-- @license MIT
-- @copyright Amora Labs 2016
--------------------------------------------------------------------------------

-- dependencies from luarocks & local
local utils = {}
local config = require 'pl.config'
local mde = require "markdown_extra"
local sailor = require 'sailor'
local md5 = require "md5"
local log = require "academy.log"
local L = require "academy.L"

-- initialize some stuff
log.outfile = "runtime/logs/lua.log" -- initialize the logfile.

local courses_config_file = 'academy/courses/courses.ini'

local is_apache = false

if sailor.r then
  is_apache = string.match(sailor.r.banner, "Apache")
end

-- Implement public API

--- normal logging
-- @param msg string
--
-- Logs *msg* to the logfile and in case of running under Apache
-- also logs to Apache log.
function utils.log(msg)
  log.debug(msg)

  if is_apache then
    sailor.r:debug(msg)
  end
end

--- error logging
-- @param error_msg string
--
-- Logs *error_msg* to the logfile and in case of running under Apache
-- also logs the error to Apache log.
function utils.error(error_msg)
  log.error(error_msg)
  if is_apache then
    --sailor.r:error(error_msg)
  end
end

--- Login/Password testing
-- Tests if a login is valid.
-- @param email string
-- @param password string
-- @return *true* if login/password is valid.
-- @return *false, msg* if the login/password is invalid.
function utils.verify_login(email, password)
  local User = sailor.model("user")
  local sha1 = require 'sha1'

  local user = User:find_by_attributes({email = email, password = sha1(password)})

  if not user then
    return false, "Usuário não encontrado!"
  else
    return user
  end
end


--- Grant access to a given user
-- This function will save a logged users data to a session thus
-- granting access to the site.
-- @param *data*, a table containing the email of the logged user
-- @param *time*[opt], how long the sessions lives, defaults to 1 week.
-- @return false, if somehow the table *data* is wrong.
-- @return the result from saving the session.
function utils.grant_access(data,time)
  local session = require "sailor.session"

  session.setsessiontimeout (time or 604800) -- 1 week
  session.open(sailor.r)


  if not data.email then
    utils.error("sessão sem email?")
    return false
  end

  utils.data = data

  return session.save(data)
end

--- avatar from gravatar for the logged user.
-- Assembles an URL for the gravatar image for the logged user.
-- @return URL for gravatar image.
-- @return nil in case there is no logged user.
function utils.logged_user_avatar_image()
  local session = require "sailor.session"

  session.setsessiontimeout(604800)
  session.open(sailor.r)

  if session.is_active() and session.data.email then
    return "https://www.gravatar.com/avatar/" .. md5.sumhexa(session.data.email:lower())
  end

  return nil
end

--- get logged user record.
-- Gets the logged user record from the database.
-- @return a table with the logged user record.
-- @return nil if no user is logged.
function utils.logged_user()
  local session = require "sailor.session"
  local model = require "sailor.model"
  local User = model("user")

  session.setsessiontimeout(604800)
  session.open(sailor.r)

  if session.is_active() and session.data.email then
    return User:find_by_email(session.data.email:lower())
  end

  return nil
end

--- get the available courses
-- This function reads from *courses.ini* config file and returns the
-- available courses. It works by checking every course in the config file
-- and fetching their config as well. Kinda intensive...
-- @return an array with the available courses.
function utils.available_courses()
  local courses = config.read(courses_config_file)
  local ret = {}
  for _,v in pairs(courses.config.courses) do
    local a_course_config_file = "academy/courses/" .. v .. "/course.ini"
    local course = config.read(L.path_with_locale(a_course_config_file))
    course.config.hero = "/academy/courses/" .. v .. "/" .. course.config.hero
    course.config.id = v
    table.insert(ret, course.config)
  end
  return ret
end

--- fetches information about a given course
-- returns information for a given course
-- @param a_course, string
-- @return a table with course data.
function utils.course(a_course)
  local a_course_config_file = "academy/courses/" .. a_course .. "/course.ini"
  local course = config.read(L.path_with_locale(a_course_config_file))

  if course == nil then
    return nil
  end

  course.config.id = a_course
  course.config.hero = "/academy/courses/" .. a_course .. "/" .. course.config.hero
  course.about_file_path =  "academy/courses/" .. a_course .. "/about.md"
  return course
end

--- fetches an unit from a given course
-- returns information for a given course unit item. Courses can have multiple units,
-- units might have multiple items.
-- @param a_course, string
-- @param an_unit, string
-- @param an_item, string
-- @return a table with item data.
function utils.item(a_course, an_unit, an_item)
  local course = utils.course(a_course)
  local unit = course["unit" .. an_unit]
  local unit_file = unit.toc[tonumber(an_item)]
  local unit_path = "academy/courses/" .. a_course .. "/" .. unit_file
  local item, meta = mde.from_file(unit_path)

  -- compute toc (kinds intensive)
  for i = 1, course.config.units do
    utils.log("unit: " .. i)
    local key = "unit" .. i
    local toc = course[key].toc
    for k = 1, #toc do
      utils.log("toc: " .. toc[k])
      local toc_item_path = "academy/courses/" .. a_course .. "/" .. toc[k]
      local _, metadata = mde.from_file(toc_item_path)

      if metadata then
        course[key].toc[k] = {title = metadata.title, item = k }
      else
        course[key].toc[k] = {title = course[key].name, item = k }
      end

      end
    utils.log("fim unit: " .. i)
  end


  return {course = course, unit = unit, item = item, meta = meta, actual = {item = an_item, unit = an_unit}}
end


--- checks if the user is at the end of the course
-- given a course, unit and item, check if the user is at the end.
--
-- It is used by the courses/unit.lp to hid the *continue* button
--
-- @param course table
-- @param unit number
-- @param item number
-- @returns boolean
function utils.is_final_unit(course, unit, item)
  unit = tonumber(unit)
  item = tonumber(item)

  if unit < course.config.units then
    return false
  end

  local key = "unit" .. tostring(unit)

  if item < #course[key].toc then
    return false
  end

  return true
end

if sailor.r then
  utils.log("Academy Server: " .. sailor.r.banner)
end

--- resets password
-- Sends an email for the given user with a password reset URL.
--
-- It relies on the [sendmail](https://luarocks.org/modules/moteus/sendmail) rock.
-- @param email string
function utils.reset_password(email)
  local sendmail = require "sendmail"
  local sha1 = require "sha1"
  local file = require "pl.file"

  local smtp_config = config.read("conf/smtp.ini")
  local from, to = 'academy@amoralabs.com', email
  local verify = sha1("isso:nao:eh:seguro:" .. email .. ":juro:para:voce")

  -- step #1: save verify file...
  file.write("runtime/tmp/" .. verify .. ".verify", email)

  -- step #2: send email...

  local ok, err = sendmail(from, to, {
    address = smtp_config.address,
    user = smtp_config.user,
    password = smtp_config.password,
    ssl = {
      protocol = "any",
      verify   = {"none"},
      options  = {"all", "no_sslv2"},
    }
  },
  {
    subject = {title = L.i18n("academy.password_reset_subject"), charset = "utf-8", encode="base64"},
    text = {data = L.i18n("academy.password_reset_body"):gsub("_verify_", verify), charset = "utf-8", encode="base64"}
  })

  if ok then utils.log("sendmail ok: " .. ok) end
  if err then utils.log("sendmail err: " .. err) end
end

--- get user from password verify feature
-- Users a ```.verify``` file to get a user.
-- @param verify_code string
-- @return user table
function utils.user_from_password_reset(verify_code)
    local file = require "pl.file"
    local model = require "sailor.model"
    local User = model("user")
    local email = file.read("runtime/tmp/" .. verify_code .. ".verify")


    if email then
      local user = User:find_by_email(email)
      if user then
        return user
      end
    end

    return nil
end

--- fix image references
-- image references in the markdown files are relative to the folder
-- they are but this does not play well with our current friendly URL
-- feature. This function helps replace the relative URLs with absolute ones.
--
-- This is used by the unit.lp view
--
-- @param prefix a string containing the prefix for the URL
-- @param body the string which is the target for our replacements
-- @return string with the strings replaced
function utils.fix_image_urls(prefix, body)
  local stringx = require "pl.stringx"
  local find = '<img src="'
  local replacement = '<img src="' .. prefix
  return stringx.replace(body, find, replacement)
end

--- @export
return utils
