--------------------------------------------------------------------------------
--
-- Academy L Library
--
-- This library has the internationalization features used by the website.
-- It uses [kikito i18n rock](https://github.com/kikito/i18n.lua)
--
-- @module academy.L
-- @author Andre Garzia <andre@amoralabs.com>
-- @license MIT
-- @copyright Amora Labs 2016
--------------------------------------------------------------------------------

local L = {}

local i18n = require "i18n"
local path = require "pl.path"
local sailor = require "sailor"
local session = require "sailor.session"


-- Initialization of the localization contexts.
i18n.loadFile('/home/amoralabs/public_html/academy/locales/en.lua')
i18n.loadFile('/home/amoralabs/public_html/academy/locales/pt.lua')
L.fallback_locale = "pt" -- used by the functions that fetches file from disk.

session.setsessiontimeout(604800) -- 1 week
session.open(sailor.r)

if session.data.locale then
    L.current_locale = session.data.locale
else
    L.current_locale = "pt"
end

i18n.setLocale(L.current_locale) -- Portuguese is the default locale.

--- Gets string in the current locale
-- uses i18n to get a given string in the current set locale. It supports
-- interpolation.
-- @param key string to look for
-- @param data[opt] table for interpolation data
-- @return translated string
function L.i18n(key, data)
    return i18n(key, data)
end

--- sets the default locale
-- changes the current locale to the given one.
-- @param locale string
function L.set_current_locale(locale)
    L.current_locale = locale
    session.data.locale = locale
    session.save(session.data)
    i18n.setLocale(L.current_locale)
end

--- returns a path modified with locale suffix
-- picks a path and using conventions, modifies it to include
-- the current locale as part of the filename.
--
-- In case the path doesn' exists it will return a path with
-- the ```fallback_locale```.
-- @param path string
-- @return path string with current locale.
-- @return fallback path, path string with the fallback locale
-- @return nil
function L.path_with_locale(file_path, fallback_locale)
    local ext = path.extension(file_path)
    local new_path = file_path:gsub(ext, "." .. L.current_locale .. ext)
    if path.exists(new_path) then
        return new_path
    end

    -- fallback! fallback!!!!
    if not fallback_locale then
        fallback_locale = L.fallback_locale
    end

    local fallback_path = file_path:gsub(ext, "." .. fallback_locale .. ext)

    if path.exists(fallback_path) then
        return fallback_path
    end
    return nil
end

-- @export
return L
