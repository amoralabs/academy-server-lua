--------------------------------------------------------------------------------
--
-- Academy Controller
--
-- This controller is the default controller of the Amora Academy portal. Most
-- features reside here with the exception of the "course taking" code which
-- resides in the *courses controller* @{courses_controller}
--
-- @module academy_controller
-- @author Andre Garzia <andre@amoralabs.com>
-- @license MIT
-- @copyright Amora Labs 2016
--------------------------------------------------------------------------------

local academy = {}
local sailor = require 'sailor'
local path = require 'pl.path'
local mde = require 'markdown_extra'
local sha1 = require 'sha1'
local utils = require 'academy.utils'
local L = require 'academy.L'

--- the main website page
-- this is function is called when the user access the root page of the website.
-- @param page, [sailor page object](http://sailorproject.org/docs/page)
function academy.index(page)
  page:render('index')
end

--- shows login page
-- @param page, [sailor page object](http://sailorproject.org/docs/page)
function academy.login_form(page)

  local redirect = page.GET.redirect or page.r.headers_in["referer"]

  page.title = "Login"
  page.subtitle = "Welcome Back to Amora Academy"

  page:render("login", {redirect_url = redirect})
end

--- renders a markdown based page
-- this is function is called when we want to present a content page. The pages
-- are stored in markdown format inside the ```academy/pages``` folder.
-- @param page, [sailor page object](http://sailorproject.org/docs/page)
function academy.view(page)
  local markdown = page.GET.page
  local markdown_file = 'academy/pages/' .. markdown ..'.md'
  local markdown_path = markdown_file

  -- check for apache and change path (Xavante doesn't need this)
  if page.r.document_root then
    markdown_path = page.r.document_root .. "/" .. markdown_file
  end

  markdown_path = L.path_with_locale(markdown_path)

  if markdown == nil or not path.exists(markdown_path) then
    utils.log("notfound: " .. markdown_path)
    return 404
  end

  local html, metadata = mde.from_file(markdown_path)

  if metadata then
    for k,_ in pairs(metadata) do
      page[k] = metadata[k]
    end
  end

  page:render('page',{body = html})
end

--- Callback from *contact form*
-- this is function is called when the user submits a contact request.
-- @param page, [sailor page object](http://sailorproject.org/docs/page)
function academy.contact(page)
  local Contact = sailor.model("contact")

  local contact = Contact:new()

  contact.name = page.POST.name
  contact.email = page.POST.email
  contact.phone = page.POST.tel
  contact.subject = page.POST.subject
  contact.msg = page.POST.msg

  local ok,err = contact:validate()

  if not ok then
    page:render("error", {err = err})
  end

  contact:save()
  page:render("page", {body = L.i18n("academy.contact_thanks")})
end

--- Callback from *signup form*
-- this is function is called when the user submits the signup form.
-- @param page, [sailor page object](http://sailorproject.org/docs/page)
function academy.signup(page)
  local User = sailor.model("user")

  local user = User:new()

  user.name = page.POST.name
  user.email = page.POST.email
  user.telefone = page.POST.tel
  user.password = sha1(page.POST.password)

  local ok,err = user:validate()

  if not ok then
    page:render("error", {err = err})
  end

  user:save()
  page:render("page", {body = L.i18n("academy.welcome")})
end

--- Callback from *update profile form*
-- this is function is called when the user submits the update profile form.
-- @param page, [sailor page object](http://sailorproject.org/docs/page)
function academy.update_profile(page)
  local user = utils.logged_user()

  if not user then
    page:redirect("academy")
  end

  user.name = page.POST.name
  user.email = page.POST.email
  user.phone = page.POST.phone
  user.password = sha1(page.POST.password)
  local confirm_password = sha1(page.POST.password)

  if user.password ~= confirm_password then
    page:render("error", {err = L.i18n("academy.password_reset_doesnt_match")})
  end

  local ok,err = user:validate()

  if not ok then
    page:render("error", {err = err})
  end

  user:save()
  page:redirect("academy/myprofile/")
end


--- Callback from *login form*
-- this is function is called when the user logs into the site.
-- @param page, [sailor page object](http://sailorproject.org/docs/page)
function academy.login(page)
  local email = page.POST.email
  local senha = page.POST.password
  local redirect = page.POST.redirect or "/academy/myprofile/"

  local ok, err = utils.verify_login(email, senha)

  if ok then
    utils.grant_access({email = email})
    page:redirect(redirect)
  else
    page:render("error", {err = L.i18n("academy.login_error") .. err})
  end
end

--- Builds the users profile page
-- this is function is called when the user goes to their profile page.
-- that page contains their personal info and the courses they are enrolled in.
-- They can use that page to alter their own data.
-- @param page, [sailor page object](http://sailorproject.org/docs/page)
function academy.myprofile(page)

  local user = utils.logged_user()


  if not user then
    utils.error("Ué, não tem user...")
    page:redirect("academy")
  end

  page:render("myprofile", {user = user})
end

--- switch site locale
-- switch current locale by saving the new locale to the current user session.
-- @param page, [sailor page object](http://sailorproject.org/docs/page)

function academy.switch_locale(page)
  local new_locale = page.GET.locale

  L.set_current_locale(new_locale)
  page:redirect(page.r.headers_in["referer"])
end

--- reset password
-- emails the user with a password reset url. (kinda dangerous)
-- @param page, [sailor page object](http://sailorproject.org/docs/page)
function academy.password_reset(page)
  local email = page.POST.email

  utils.reset_password(email)
  page:render("page", {body = L.i18n("academy.password_reset_mail_sent")})
end

--- reset password form
-- Displays the form used for password reset
-- @param page, [sailor page object](http://sailorproject.org/docs/page)
function academy.password_reset_form(page)
  local verify = page.GET.verify

  local user = utils.user_from_password_reset(verify)

  if user then
    page:render("password_reset", {user = user, verify = verify})
  else
    page:render("error", {err = L.i18n("academy.password_reset_verify_error")})
  end
end

--- callback for the password reset form
-- called when the user submits a password reset form
-- @param page, [sailor page object](http://sailorproject.org/docs/page)
function academy.reset_password_callback(page)
  local verify = page.POST.verify
  local new_password = page.POST.password
  local password_confirm = page.POST.confirm_password

  local user = utils.user_from_password_reset(verify)

  if not user then
    page:render("error", {err = L.i18n("academy.password_reset_verify_error")})
  end

  if new_password ~= password_confirm then
    page:render("error", {err = L.i18n("academy.password_reset_doesnt_match")})
  end

  user.password = sha1(new_password)

  local ok,err = user:validate()

  if not ok then
    page:render("error", {err = err})
  end

  user:save()
  page:render("page", {body = L.i18n("academy.password_reset_ok")})

end

-- @export
return academy
