--------------------------------------------------------------------------------
--
-- Enrollment Model
--
-- This model represents a users enrollment in a given course.
--
-- @module enrollment_model
-- @author Andre Garzia <andre@amoralabs.com>
-- @license MIT
-- @copyright Amora Labs 2016
--------------------------------------------------------------------------------
local val = require "valua"

local progress = {}
-- Attributes and their validation rules
progress.attributes = { 
  { progress_id = "safe" }, -- No validation rules
  { user_id = val:new().not_empty().integer() },
  { course = val:new().not_empty().len(0,255) }, 
  { unit = val:new().not_empty().integer() }, 
  { item = val:new().not_empty().integer() }, 
  { date = "safe"}
}

progress.db = {
  key = 'progress_id', -- the primary key
  table = 'progress' -- make sure this field contains the same name as your SQL table!
}

progress.relations = {
  user = {relation = "HAS_ONE", model = "user", attribute = "user_id"}
}

return progress