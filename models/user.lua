--------------------------------------------------------------------------------
--
-- User Model
--
-- This model represents a user along with its course enrollment and progress.
--
-- @module user_model
-- @author Andre Garzia <andre@amoralabs.com>
-- @license MIT
-- @copyright Amora Labs 2016
--------------------------------------------------------------------------------
local val = require "valua"
local sha1 = require "sha1"
local md5 = require "md5"
local sailor = require "sailor"
local utils = require "academy.utils"
local pretty = require "pl.pretty"

local user = {}
-- Attributes and their validation rules
user.attributes = { 
-- { <attribute> = <validation function, valua required> or "safe" if no validation required for this attribute} 
  { user_id = "safe" }, -- No validation rules
  { name = val:new().not_empty().len(0,255) }, -- Cannot be empty and must have between 6 and 20 characters
  { email = val:new().not_empty().len(0,255) }, -- Cannot be empty and must have between 6 and 100 characters
  { password = val:new().not_empty() }, -- Cannot be empty
  { phone = "safe"},
  { level = "safe"}
}

user.db = {
  key = 'user_id', -- the primary key
  table = 'user' -- make sure this field contains the same name as your SQL table!
}

user.relations = {
  progress_entries = {relation = "HAS_MANY", model = "progress", attribute = "user_id"},
  enrollments = {relation = "HAS_MANY", model = "enrollment", attribute = "user_id"}
}

--- checks if the password is correct
-- Verifies if the password for the model matches a given password. It checks using sha1
-- @param password a string containing the password
-- @return boolean
function user:is_password_correct(a_password) 
  local hash_password = sha1(a_password)
  return self.password == hash_password
end 

--- gravatar image for users avatar
-- assembles and returns a string containing a link to an image on gravatar
-- @return url string with link to image
function user:avatar()
  return "https://www.gravatar.com/avatar/" .. md5.sumhexa(self.email:lower())
end

--- finds user by email
-- just a handy method to lookup users by email.
-- @param email string
-- @return user model
function user:find_by_email(email)
  local attributes = {email = email}
  return self:find_by_attributes( attributes )
end


--- checks if user is enrolled in course
-- @param course
-- @return boolean
function user:is_enrolled(course)

  -- not enrolled in any course
  if #self.enrollments == 0 then
    return false
  end

  -- enrolled in some courses...
  for i=1,#self.enrollments do
    if self.enrollments[i].course == course.config.id then
      -- found enrollment for given course.
      return true
    end
  end

  -- nope, not enrolled...
  return false
end

--- enrolls a user into a given course
-- Adds a record to the enrollment table with the user and the course
-- @param course course model instance
-- @return ok true
-- @return nil, error
function user:enroll(course)
  local Enrollment = sailor.model("enrollment")

  -- check if the user has previous enrollment.
  local is_enrolled = self:is_enrolled(course)

  if is_enrolled then
    return true
  end

  utils.log("enroll for user: " .. self.user_id)
  -- user is not enrolled... create new record.
  local enrollment = Enrollment:new()
  enrollment.user_id =  tonumber(self.user_id)
  enrollment.course = course.config.id
  Enrollment.date = os.date("!%Y-%m-%d %T")
  
  local ok,err = enrollment:validate()

    if not ok then
      utils.error(err)
      return nil, err
    end

    enrollment:save() 

    return true
end

--- annonates users progress
-- Adds a record to the progress table with the user progress
-- @param obj table from ```utils.item```
-- @return ok true
-- @return nil, error
function user:progress(obj)
  local Progress = sailor.model("progress")
  local course = obj.course

  -- check if the user has previous progress.
  local is_enrolled = self:is_enrolled(course)

  if not is_enrolled then
    return false
  end

  local progress = Progress:new()
  progress.user_id =  tonumber(self.user_id)
  progress.course = course.config.id
  progress.unit = tonumber(obj.actual.unit)
  progress.item = tonumber(obj.actual.item)
  progress.date = os.date("!%Y-%m-%d %T")
  
  local ok,err = progress:validate()

    if not ok then
      utils.error(err)
      return nil, err
    end

    progress:save() 

    return true
end

--- gets the furthest position on a given course
-- @param course the obj for the course
-- @return obj table with data
-- @return error boolean as second return value.
function user:furthest_item_for_course(course)
  local position = {furthest_unit = 1, furthest_item = 1} -- default values.

  if not self:is_enrolled(course) then
    return nil, false
  end

  -- find furthest unit
  for i=1,#self.progress_entries do
    local progress = self.progress_entries[i]

    if progress.course == course.config.id then
      -- user had progress for course.
      position.furthest_unit = math.max(position.furthest_unit, progress.unit)
    end
  end

  -- find furthest item
  for i=1,#self.progress_entries do
    local progress = self.progress_entries[i]

    if progress.course == course.config.id and position.furthest_unit == progress.unit then
      -- user had progress for course.
      position.furthest_item = math.max(position.furthest_item, progress.item)
    end
  end

  return position, nil
end

return user