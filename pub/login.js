$('#cancel-button').click(function(e) {
    e.preventDefault();
    window.history.back();
});

$('#submit-button').click(function(e) {
    e.preventDefault();
    $("#login-form").submit();
});